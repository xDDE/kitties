-- Initial DB data to start with

insert into kitty (name, rating, img_url) values ('Мурзик',   0, '/img/kitties/Murzik.jpg');
insert into kitty (name, rating, img_url) values ('Барсик',   0, '/img/kitties/Barsik.jpg');
insert into kitty (name, rating, img_url) values ('Муся',     0, '/img/kitties/Musya.jpg');
insert into kitty (name, rating, img_url) values ('Пушок',    0, '/img/kitties/Pushok.jpg');
insert into kitty (name, rating, img_url) values ('Бармалей', 0, '/img/kitties/Barmalei.jpg');