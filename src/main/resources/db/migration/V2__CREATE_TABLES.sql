-- Initial DB tables

create table kitty (
    id      bigserial primary key,
    img_url varchar(255) not null,
    name    varchar(255) not null,
    rating  bigint default 0
);
alter table kitty
    owner to kitties_db_user;

create table voting (
    id           bigserial primary key,
    session_id   varchar(255) not null constraint uk_voting_session_id unique,
    viewed_pairs varchar
);
alter table voting
    owner to kitties_db_user;