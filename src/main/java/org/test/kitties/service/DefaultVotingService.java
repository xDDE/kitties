package org.test.kitties.service;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.test.kitties.db.entity.VotingEntity;
import org.test.kitties.db.repo.VotingRepository;
import org.test.kitties.model.VotingModel;

import java.util.Collections;

@Service
@RequiredArgsConstructor
public class DefaultVotingService implements VotingService {
    private final VotingRepository votingRepository;

    @NonNull @Override
    public VotingModel create(@NonNull VotingModel voting) {
        VotingEntity votingEntity = votingRepository.save(voting.toEntity());
        return VotingModel.toModel(votingEntity);
    }

    @Nullable @Override
    public VotingModel update(Long votingId, @NonNull VotingModel voting) {
        if(votingId == null)
            return null;
        VotingEntity votingEntity = votingRepository.findById(votingId).orElse(null);
        if (votingEntity == null)
            return null;
        votingEntity = votingRepository.save(voting.toEntity());
        return VotingModel.toModel(votingEntity);
    }

    @NonNull @Override
    @Transactional
    public VotingModel getBySessionIdOrCreate(@NonNull String sessionId) {
        VotingEntity votingEntity = votingRepository.findBySessionId(sessionId);
        if(votingEntity == null)
            return new VotingModel(sessionId, Collections.emptySet());
        return VotingModel.toModel(votingEntity);
    }
}
