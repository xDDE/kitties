package org.test.kitties.service;

import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.test.kitties.model.KittyModel;

import java.util.List;

public interface KittyService {
    @Nullable
    KittyModel update(Long kittyId, @NonNull KittyModel kitty);

    @NonNull
    List<KittyModel> findAll();

    @Nullable
    KittyModel findById(Long idKitty);
}
