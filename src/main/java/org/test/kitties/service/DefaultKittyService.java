package org.test.kitties.service;

import lombok.RequiredArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.test.kitties.db.entity.KittyEntity;
import org.test.kitties.db.repo.KittyRepository;
import org.test.kitties.model.KittyModel;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class DefaultKittyService implements KittyService {
    private final KittyRepository kittyRepository;
    private Set<String> st;

    @Nullable @Override
    public KittyModel update(Long kittyId, @NonNull KittyModel kitty) {
        if(kittyId == null)
            //st.ad
            return null;
        KittyEntity kittyEntity = kittyRepository.findById(kittyId).orElse(null);
        if (kittyEntity == null)
            return null;
        kittyEntity = kittyRepository.save(kitty.toEntity());
        return KittyModel.toModel(kittyEntity);
    }

    @NonNull
    @Override
    @Transactional(readOnly = true)
    public List<KittyModel> findAll() {
        return kittyRepository.findAll()
                .stream()
                .map(KittyModel::toModel)
                .collect(Collectors.toList());
    }

    @Nullable
    @Override
    public KittyModel findById(Long idKitty) {
        if(idKitty == null)
            return null;
        KittyEntity kittyEntity = kittyRepository.findById(idKitty).orElse(null);
        if(kittyEntity == null)
            return null;
        return KittyModel.toModel(kittyEntity);
    }
}
