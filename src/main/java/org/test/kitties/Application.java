package org.test.kitties;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Application {
	public static void main(String[] args) {
		System.setProperty("log4j.skipJansi", "false");  // enable console output colorize after switching to log4j2 logger

		SpringApplication.run(Application.class, args);
	}
}
