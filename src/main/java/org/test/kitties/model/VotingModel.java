package org.test.kitties.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.lang.NonNull;
import org.springframework.lang.Nullable;
import org.test.kitties.db.entity.VotingEntity;
import org.test.kitties.util.KittyPair;

import java.util.*;
import java.util.stream.Collectors;

@Data
@NoArgsConstructor
public class VotingModel {
    public VotingModel(String sessionId, Set<KittyPair> viewedPairs) {
        this.sessionId = sessionId;
        this.viewedPairs = viewedPairs;
    }

    private Long id;

    private String sessionId;

    private Set<KittyPair> viewedPairs;

    public static VotingModel toModel(@NonNull VotingEntity votingEntity) {
        VotingModel voting = new VotingModel();
        voting.setId(votingEntity.getId());
        voting.setSessionId(votingEntity.getSessionId());
        voting.setViewedPairs(parseViewedPairs(votingEntity.getViewedPairs()));
        return voting;
    }

    public VotingEntity toEntity() {
        VotingEntity votingEntity = new VotingEntity();
        votingEntity.setId(this.getId());
        votingEntity.setSessionId(this.getSessionId());
        votingEntity.setViewedPairs(serializeViewedPairs(this.getViewedPairs()));
        return votingEntity;
    }

    private static Set<KittyPair> parseViewedPairs(@Nullable String strViewedPairs) {
        if(strViewedPairs == null)
            return Collections.emptySet();

        Set<KittyPair> setViewedPairs = new HashSet<>();
        String[] viewedPairsArray = strViewedPairs.split(",");

        try {
            for (String pair : viewedPairsArray) {
                String[] pairArray = pair.split(":");
                if (pairArray.length == 2) {
                    Long id1 = Long.parseLong(pairArray[0]);
                    Long id2 = Long.parseLong(pairArray[1]);
                    KittyModel kitty1 = new KittyModel();
                    KittyModel kitty2 = new KittyModel();
                    kitty1.setId(id1);
                    kitty2.setId(id2);
                    setViewedPairs.add(new KittyPair(kitty1, kitty2));
                }
            }
        } catch (NumberFormatException nfe) {
            throw new VotingModelParseException("Wrong kitty pairs format appeared during parsing");
        }
        return setViewedPairs;
    }

    private static String serializeViewedPairs(Set<KittyPair> setViewedPairs) {
        List<String> listIdPairs = setViewedPairs.stream()
                .map(pair -> pair.getFirst().getId() + ":" + pair.getSecond().getId())
                .collect(Collectors.toList());

        return String.join(",", listIdPairs);
    }

    private static class VotingModelParseException extends RuntimeException {
        public VotingModelParseException(String message) {
            super(message);
        }
    }
}
