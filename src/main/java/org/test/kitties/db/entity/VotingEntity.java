package org.test.kitties.db.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "voting")
public class VotingEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "session_id", nullable = false, unique = true)
    private String sessionId;

    @Lob  // unlimited length
    @Column(name = "viewed_pairs")
    private String viewedPairs;  // format: "[id1:id2,id3:id8,...,id8:id1]"
}
