package org.test.kitties.db.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.test.kitties.db.entity.KittyEntity;

public interface KittyRepository extends JpaRepository<KittyEntity, Long> { }