package org.test.kitties.controller;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.test.kitties.model.KittyModel;
import org.test.kitties.model.VotingModel;
import org.test.kitties.service.KittyService;
import org.test.kitties.service.VotingService;
import org.test.kitties.util.TopBuilder;
import org.test.kitties.util.VoteManager;
import org.test.kitties.util.VotingSequence;
import org.thymeleaf.util.StringUtils;

import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@AllArgsConstructor
@Log4j2
public class AppController {
    private final KittyService kittyService;
    private final VotingService votingService;

    @GetMapping("/")       // web-site root
    public String home(HttpSession session, Model model) {
        //session.removeAttribute("isVotingCompleted");

        if(Boolean.TRUE.equals(session.getAttribute("isVotingCompleted")))
            return "redirect:/completed";

        VotingModel voting = votingService.getBySessionIdOrCreate(session.getId());
        VotingSequence votingSequence = getVotingSequence(voting);

        if(votingSequence.isVotingCompleted()) {
            session.setAttribute("isVotingCompleted", true);
            return "redirect:/completed";
        }

        model.addAttribute("progress", votingSequence.getProgress());
        model.addAttribute("nextPair", votingSequence.getNextPair());

        return "vote";
    }

    @GetMapping("/vote")
    public String vote(@RequestParam(name="for") String forId, @RequestParam(name="against") String againstId,
                       HttpSession session, Model model) {

        if(Boolean.TRUE.equals(session.getAttribute("isVotingCompleted")))
            return "redirect:/completed";

        String strVoteResult = doVote(forId, againstId, session, model);
        if(!StringUtils.isEmpty(strVoteResult))
            return strVoteResult;

        session.setAttribute("isVotingCompleted", true);
        return "redirect:/completed";
    }

    @GetMapping("/completed")
    public String completed(Model model) {
        List<KittyModel> listTopKitties = TopBuilder.build(kittyService, 10);
        model.addAttribute("listTopKitties", listTopKitties);
        return "completed";
    }

    @NonNull
    synchronized private String doVote(String forId, String againstId, HttpSession session, Model model) {
        // synchronized: protect KittyEntity.rating++ from another thread impact

        VoteManager voteManager = new VoteManager(forId, againstId, session, kittyService, votingService);
        if(voteManager.hasSomeError()) {
            if(voteManager.isAlreadyVotedPair())
                return alreadyVotedPair(voteManager.getErrorMessage(), model);
            return error(voteManager.getErrorMessage(), model);
        }

        VotingSequence votingSequence = getVotingSequence(voteManager.getVoting());
        if(!votingSequence.isVotingCompleted()) {

            VotingModel voting = voteManager.vote();  // synchronized: update rating
            votingSequence = getVotingSequence(voting);

            if(!votingSequence.isVotingCompleted()) {
                model.addAttribute("progress", votingSequence.getProgress());
                model.addAttribute("nextPair", votingSequence.getNextPair());
                return "vote";
            }
        }
        return "";
    }

    private String error(String errorMessage, Model model) {
        model.addAttribute("errorMessage", errorMessage);
        log.trace("Custom error: " + errorMessage);
        return "error/custom";
    }

    private String alreadyVotedPair(String errorMessage, Model model) {
        model.addAttribute("errorMessage", errorMessage);
        log.trace("Custom error: " + errorMessage);
        return "error/already-voted-pair";
    }

    private VotingSequence getVotingSequence(VotingModel voting) {
        return new VotingSequence(kittyService, voting);
    }
}
