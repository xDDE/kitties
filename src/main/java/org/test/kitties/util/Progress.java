package org.test.kitties.util;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Progress {
    private int used;
    private int total;

    public int getPercent() {
        if(total == 0)
            return 100;
        return Math.round((float)(used + 1) / (float)total * 100);
    }

    @Override
    public String toString() {
        return (used + 1) + " из " + total;
    }
}
