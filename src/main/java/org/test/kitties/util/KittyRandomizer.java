package org.test.kitties.util;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class KittyRandomizer {
    private final List<KittyPair> listUnusedPairs;

    public static KittyRandomizer from(List<KittyPair> listUnusedPairs) {
        return new KittyRandomizer(listUnusedPairs);
    }

    public KittyRandomizer(List<KittyPair> listUnusedPairs) {
        this.listUnusedPairs = listUnusedPairs;
    }

    public KittyPair getRandom() {
        int index = ThreadLocalRandom.current().nextInt(0, listUnusedPairs.size());
        return listUnusedPairs.get(index);
    }
}
