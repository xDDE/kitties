package org.test.kitties.util;

import lombok.Getter;
import org.test.kitties.model.KittyModel;

import java.util.Objects;

@Getter
public class KittyPair {
    private final KittyModel kitty1;
    private final KittyModel kitty2;

    private final KittyModel first;
    private final KittyModel second;

    public KittyPair(final KittyModel kitty1, final KittyModel kitty2) {
        this.kitty1 = kitty1;
        this.kitty2 = kitty2;
        this.first = findFirst(kitty1, kitty2);
        this.second = findSecond(kitty1, kitty2);
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, second);
    }

    @Override
    public boolean equals(final Object other) {
        if(this == other)
            return true;

        if(! ( other instanceof KittyPair) )
            return false;

        final KittyPair otherPair = (KittyPair) other;
        return this.first.equals(otherPair.first) && this.second.equals(otherPair.second);
    }

    private KittyModel findFirst(final KittyModel kitty1, final KittyModel kitty2) {
        return kitty1.getId() < kitty2.getId() ? kitty1 : kitty2;
    }

    private KittyModel findSecond(final KittyModel kitty1, final KittyModel kitty2) {
        return kitty1.getId() > kitty2.getId() ? kitty1 : kitty2;
    }
}
