package org.test.kitties.util;

import lombok.Getter;
import org.test.kitties.model.KittyModel;
import org.test.kitties.model.VotingModel;
import org.test.kitties.service.KittyService;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Getter
public class VotingSequence {
    private final KittyPair nextPair;
    private final Progress progress;
    private final Set<KittyPair> setUnusedPairs;

    public VotingSequence(KittyService kittyService, VotingModel voting) {

        Set<KittyPair> setAllPairs = getAllPairs(kittyService.findAll());

        List<KittyPair> listUsedPairs = new ArrayList<>(voting.getViewedPairs());
        setUnusedPairs = new HashSet<>(setAllPairs);
        listUsedPairs.forEach(setUnusedPairs::remove);

        int total = setAllPairs.size();

        if(setUnusedPairs.isEmpty()) {
            progress = new Progress(total, total);
            nextPair = null;
        } else {
            progress = new Progress(listUsedPairs.size(), total);
            nextPair = KittyRandomizer.from(new ArrayList<>(setUnusedPairs)).getRandom();
        }
    }

    private Set<KittyPair> getAllPairs(List<KittyModel> listAllKitties) {
        Set<KittyPair> setAllPairs = new HashSet<>();

        for (KittyModel kitty1 : listAllKitties)
            for (KittyModel kitty2 : listAllKitties)
                if (!kitty1.equals(kitty2))
                    setAllPairs.add(new KittyPair(kitty1, kitty2));  // (kitty1, kitty2) is the same as (kitty2, kitty1)

        return setAllPairs;
    }

    public boolean isVotingCompleted() {
        return setUnusedPairs.isEmpty();
    }
}
