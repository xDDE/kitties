package org.test.kitties.util;

import org.test.kitties.model.KittyModel;
import org.test.kitties.service.KittyService;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class TopBuilder {
    public static List<KittyModel> build(KittyService kittyService, int maxSize) {
        List<KittyModel> listAllKitties = new ArrayList<>(kittyService.findAll());
        return listAllKitties.stream()
                .sorted(Comparator.comparing(KittyModel::getRating).reversed())
                .limit(maxSize)
                .collect(Collectors.toList());
    }
}
